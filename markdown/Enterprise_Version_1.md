---
date: 2020-02-11
---
I like Star Trek. I like the original series quite a lot. Last night while watching a movie (It was Hunger games: Catching fire) I made this. I think its pretty good first draft.

![](../images/mywork/ncc-NoA2.gif)

Ill take another pass at this later this evening, see if I can refine the animation a little. The stars are a bit too blinky and I should stagger the movement by a frame or so. But over all I'm happy with it.


