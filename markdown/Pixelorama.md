---
date: 2020-01-08
---
	 888888ba  dP dP    dP  88888888b dP        .d88888b
	 88    `8b 88 Y8.  .8P  88        88        88.    "'
	a88aaaa8P' 88  Y8aa8P  a88aaaa    88        `Y88888b.
	 88        88 d8'  `8b  88        88              `8b
	 88        88 88    88  88        88        d8'   .8P
	 dP        dP dP    dP  88888888P 88888888P  Y88888P

Its true. I like to pixel. But, truth be told. I have only ever slammed dots down in 2 applications. Gimp (while very zoomed in) and Aseprite.

The reason for this, in all honestly is that Aseprite has everything I could ever imagine that I will need in my Pointillism adventures.

But after a [Gaming on Linux Article](https://www.gamingonlinux.com/articles/pixelorama-the-free-and-open-source-sprite-editor-has-a-sweet-new-release-up.15725) about this newer Free Software application called Pixelorama I decided to five it a look.

## First Impressions.

The application looks really nice. Given that Aseprite looks like its aimed at a younger audience (its not, it just looks like that) and hides its breadth of tools behind little cartoony buttons, this new more serious tone was refreshing.

Dark theme by default, also nice.

As I moved the mouse around I noticed a floating icon each side of the cursor, did not like that.

There are only the most basic of tools. No advanced shape or selection tools. One of the things it does do that's nice is that it allow you to put any tool on any mouse button, I can map erase to left or right or brush to right. It's a nice way of doing things.

Timeline tools are also very basic compared to Aseprite but seem to have all of the actual fundamentals down.

## The Limits?

The lack of more advanced tools quickly irritated me. While I *can* manually do anything I really need with a pencil tool I like the automation that Aseprite has. For instance if I draw a sprite  and want to give it a white edge, Aseprite as a tool for that, in Pixelorama I have to do it manually and that takes time, pointlessly.

The Animation time-line seems to have implemented onions skinning really well (how a ghost of previous/next frame on current frame) but also screwed up some colours when I began working on more filled images. A Per-layer selection would have helped there.

The options menu is limited to say the least.

## AND?

When Pixelorama has been in the over for another few releases it will be a GREAT choice for any buddy pixel-picasso. If it carries on in this trajectory I could even see it becoming an excellent choice for even the most advanced user by version 1.0 (assuming they use sensible version numbering)

[Get Pixelorama here](https://www.orama-interactive.com/pixelorama)

Thats all... I'm bored of words now.

<div class="xpeng">
<iframe width="560" height="315" src="https://www.youtube.com/embed/4IF0eh_jEJA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
