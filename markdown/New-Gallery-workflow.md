---
date: 2019-12-11
---
Hey all!

Just a workflow/website update here. I realise that at this point this blog is a stream of conciousness more than a traditional information source but I'm now able to add images to the gallery by dropping it in a folder and running a script. Because of this I'm making it the focus point on the site rather than the blog feed its self. This way I'm more likely to update the site as well as more likely to share my half formed ideas and images.

All gallery images are now webp format, its not a well known image format but it has great compression and loads real fast! - the site could still be a bear in low powered hardware though. It's a gallery, there are lots of images to load. You can click on the images to get a full size version (as I work in 320x180 most the time, theres not much point in zooming in on the native size ones)

Anyway, Gallery if front and center on the main pixelfridge.club website. You pretty much cant miss it!



