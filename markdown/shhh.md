---
date: 2020-08-12
---
# Do I need two sites?

The reality is that when I started the Pixel Fridge I was working pretty hard on some stuff and it seemed to me that I needed two sites.

The reality is, I don't.

HexDSL.CO.UK is, I'm sure big enough to hold my blog AND my artwork. I think ill have to setup a second gallery there, but keeping it all in one place seems like the right thing to do.

Not de-focusing my pixel slamming adventures, just moving them to the same place as everything else.

Anyway. Ill be working on that in the next few days.

Thanks.

